Feature: Event ticket system

Simple system that uses Cardano tokens to manage the sale of event tickets

@mytag
Scenario: Band gives a concert
Given I have an concert next Sunday
And I have <Seats available>
When Mint 1 <NFT> on Cardano for each of the available seats  
Then I should be able to sell them for <Price>

@mytag
Scenario Outline: Fan books seat for event
Given Visits festival website
And Wants to book a seat for <Event>
When He sends <Ticket Booking Cost> to <Event Treasury> from <Fan Address>
Then System should bind <Fan Address> with <Seat>


@mytag
Scenario Outline: Fan attends to event
Given Fan holds a
And Wants to book a seat for <Event>
When He sends <Ticket Booking Cost> to <Event Treasury> from <Fan Address>
Then He should receive a booking <NFT> on <Fan Address>


Examples:
    | Seats available | NFT            | Price  | Event | Ticket Booking Cost | Event Treasury | Fan Address | Seat    |
    | 100             | policyId.rave0 | 1		| rave0	| 1000000 lovelace    | addr1...	   | addr1...	 | seatid1 |