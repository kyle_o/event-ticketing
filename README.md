# Gimbalabs Ticketing Dapp

See [this Miro board](https://miro.com/app/board/o9J_lR3E3vw=/) for up to the minute ideas and work in progress.

## Outcomes
1. Onboard new developers to Cardano
2. Begin to define how work gets done on distributed teams at Gimbalabs.
3. Learn how to get experienced devs up to speed when they're new to blockchain
4. Create an event ticketing service built on Cardano native assets and transaction metadata
5. Create processes and microservices to support #4.

## Why are doing this?
1. Event tickets serve as real world examples of NFT
2. Example of disintermediation - both opportunity and responsibility/support
3. By building what we can right now with metadata and native assets, we will
    - Create tools and microservices to support additional projects
    - Build a shared understanding of how smart contracts will help us to automate distribution of tickets (and more)

## What is the Gimbalabs Playground?
[Link to gimbalabs.com/playground](https://gimbalabs.com/playground)

## How will this project contribute to the Gimbalabs?



## Meeting List:
- Weekly Full Team - Tuesdays at 4pm UTC: https://zoom.us/j/93277716737
- Next dev Meeting
- Next Comms meeting

- [Clone diagram on draw.io](https://app.diagrams.net/?libs=general;flowchart#Uhttps%3A%2F%2Fgitlab.com%2Fgimbalabs%2Fplayground%2Fevent-ticketing%2F-%2Fraw%2Fmaster%2Fevent-ticketing-system.drawio%3Finline%3Dfalse)
